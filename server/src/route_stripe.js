const stripe = require("stripe")(process.env.STRIPE_SECRET_API_KEY);

module.exports = {
  charge: async (req, res) => {
    try {
      const { amount, source, receipt_email, currency, description } = req.body;

      const charge = await stripe.charges.create({
        amount,
        currency: currency || "usd",
        source,
        description,
        receipt_email,
      });

      if (!charge) throw new Error("charge unsuccessful");

      res.status(200).json({
        message: "charge posted successfully",
        charge,
      });
    } catch (error) {
      res.status(500).json({
        message: error.message,
      });
    }
  },
};
