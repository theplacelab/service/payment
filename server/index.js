"use strict";
require("dotenv").config({ path: require("find-config")(".env") });
const middleware = require("./src/middleware.js");
const express = require("express");
const cors = require("cors");
const bodyParser = require("body-parser");
const route_stripe = require("./src/route_stripe.js");
const app = express();
const fs = require("fs");
const https = require("https");
const port = process.env.PORT || 8888;
const path = require("path");

console.log(`\n--------------------------------------`);
console.log(`Stripe Payment Support Server`);
console.log(`Port: ${port}`);
console.log(`--------------------------------------`);

app.use(cors());
app.use(express.json());
app.use(express.static("public"));
app.engine("html", require("ejs").renderFile);

try {
  let certificate = fs.readFileSync("/cert/fullchain.pem");
  let privateKey = fs.readFileSync("/cert/privkey.pem");
  https
    .createServer(
      {
        key: privateKey,
        cert: certificate,
      },
      app
    )
    .listen(port);
  console.log("SSL: ENABLED");
} catch {
  console.log("SSL: DISABLED - CERTIFICATE NOT FOUND");
  app.listen(port);
}

app.post("/stripe/charge", (req, res) => {
  route_stripe.charge(req, res);
});

app.get("*", (req, res) => {
  res.sendFile(path.join(__dirname, "public", "index.html"));
});

// Fallthrough
app.use(function (req, res) {
  console.log(`REJECTING: ${req.method}:${req.originalUrl}`);
  res.sendStatus(404);
});
