# Stripe Payments (Tipjar)

☕ &nbsp; _Is this project helpful to you? [Please consider buying me a coffee](https://pay.feralresearch.org/tip)._

A React front-end with a companion Node server that accepts credit cards for Stripe. Currently configured only for "Tip Jar" could be modified to invoice any amount.

Both are built into a single container intended to run on the same domain, should be configured using .env file. I am running this using a single api server to deliver the React app, but if you prefer to split them that should be relatively easy, make two `.env` files and build them indepedently.

Note: You MUST serve this via SSL for live payments to work

`client`  
Directory contains the React client to collect info from the user and sign via stripe API using the public key.

`server`  
Directory contains the node server that accepts the token from the front end and actually does the charge using the private key.

`.env`  
Contains your keys and confguration options for both client and server

## Container Image

This repo packages itself into an image you can use with K8 or Docker (sample config below). You can use: `registry.gitlab.com/theplacelab/service/payment`

### Sample K8 Config

```yml
apiVersion: v1
kind: Service
metadata:
  name: payment
spec:
  ports:
    - port: 80
      targetPort: 8888
  selector:
    app: payment
    component: payment
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: payment
spec:
  replicas: 2
  selector:
    matchLabels:
      app: payment
      component: payment
  template:
    metadata:
      labels:
        component: payment
    spec:
      containers:
        - name: payment
          image: registry.gitlab.com/theplacelab/service/payment
          imagePullPolicy: Always
          ports:
            - containerPort: 8888
          env:
            - name: STRIPE_SECRET_API_KEY
              value: sk_****************
            - name: REACT_APP_STRIPE_PUBLIC_API_KEY
              value: pk_****************
            - name: REACT_APP_PAYMENT_API
              value: http://example.com
            - name: REACT_APP_TIP_SUGGESTED_AMOUNTS
              value: "5.00,10.00,15.00,20.00"
            - name: REACT_APP_TIP_SUGGESTED_DEFAULT
              value: "10.00"
            - name: REACT_APP_TIP_HEADLINE
              value: "Thank You!"
            - name: REACT_APP_TIP_MESSAGE
              value: "Thank you for your support!"
            - name: REACT_APP_TIP_BILLING_AGENT
              value: "My Company Co."
            - name: REACT_APP_TIP_ALLOW_OTHER
              value: "true"
            - name: REACT_APP_TIP_RECEIPT_DESCRIPTION
              value: "Thanks for the tip"
            - name: REACT_APP_TIP_ROUTE
              value: "/tip/company"
            - name: REACT_APP_REDIRECT
              value: "https://example.com"
```

### Licence / Credits

- [MIT License](/LICENSE)
- This project makes use of FontAwesome icons under the [Creative Commons Attribution 4.0 International license](https://fontawesome.com/license)

- Thanks to the following tutorials for getting me started:  
  https://patmurray.co/words/collecting-tips-with-stripe-apple-pay/  
  https://blog.logrocket.com/building-payments-system-react-stripe/
