// This is a generated file, do not edit
window._env_ = {
  REACT_APP_STRIPE_PUBLIC_API_KEY: 'pk_********************',
  REACT_APP_PAYMENT_API: 'http://localhost:8888',
  REACT_APP_TIP_SUGGESTED_AMOUNTS: '5.00,10.00,15.00,20.00',
  REACT_APP_TIP_SUGGESTED_DEFAULT: '10.00',
  REACT_APP_TIP_HEADLINE: 'Thank You!',
  REACT_APP_TIP_MESSAGE: 'Thank you for your support!',
  REACT_APP_TIP_BILLING_AGENT: 'Company Inc. Co.',
  REACT_APP_TIP_ALLOW_OTHER: 'true',
};
